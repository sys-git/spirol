.. :changelog:

Changelog
=========

1.0.8 2015-01-09
----------------
* Updated readme

1.0.8 2015-01-09
----------------
* update

1.0.7 2015-01-09
----------------
* fixed setup

1.0.6 2015-01-09
----------------
* Updated tagger to publish to pypi

1.0.5 2015-01-09
----------------
* updated tagger

1.0.4 2015-01-09
----------------
* updated tagger

1.0.3 2015-01-09
----------------
* Updated readme

1.0.2 2015-01-09
----------------
* Updated readme to pull in shippable status.

1.0.1 2015-01-09
----------------
* Added tox, versioneer

1.0.0 2014-11-19
----------------
* Production stable


