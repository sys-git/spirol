futures>=2.2.0
nose>=1.3.1
coverage>=3.7.1
shifty==0.0.3
tox==1.8
sh>=1.11
click>=3.3
