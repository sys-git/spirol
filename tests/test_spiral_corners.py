#!/usr/bin/env python
#    -*- coding: utf-8 -*-

import unittest

from spirol.impls.spirol_array_iter import spirol_array_iter
from spirol.utils import SpirolDirection


class verify_all_corners(unittest.TestCase):
    def _scan(self, data, size, **kwargs):
        return spirol_array_iter(data, size, **kwargs).scan()

    def execute_4x4(self, size=(4, 4), eResult=None, **kwargs):
        data = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_4x3(self, size=(4, 3), eResult=None, **kwargs):
        data = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_4x2(self, size=(4, 2), eResult=None, **kwargs):
        data = [[1, 2], [3, 4], [5, 6], [7, 8]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_4x1(self, size=(4, 1), eResult=None, **kwargs):
        data = [[1], [2], [3], [4]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_1x1(self, size=(1, 1), eResult=None, **kwargs):
        data = [[1]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_1x2(self, size=(1, 2), eResult=None, **kwargs):
        data = [[1, 2]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_1x3(self, size=(1, 3), eResult=None, **kwargs):
        data = [[1, 2, 3]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_1x4(self, size=(1, 4), eResult=None, **kwargs):
        data = [[1, 2, 3, 4]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_2x1(self, size=(2, 1), eResult=None, **kwargs):
        data = [[1], [2]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_2x2(self, size=(2, 2), eResult=None, **kwargs):
        data = [[1, 2], [3, 4]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_2x3(self, size=(2, 3), eResult=None, **kwargs):
        data = [[1, 2, 3], [4, 5, 6]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_2x4(self, size=(2, 4), eResult=None, **kwargs):
        data = [[1, 2, 3, 4], [5, 6, 7, 8]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_3x1(self, size=(3, 1), eResult=None, **kwargs):
        data = [[1], [2], [3]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_3x2(self, size=(3, 2), eResult=None, **kwargs):
        data = [[1, 2], [3, 4], [5, 6]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_3x3(self, size=(3, 3), eResult=None, **kwargs):
        data = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def execute_3x4(self, size=(3, 4), eResult=None, **kwargs):
        data = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
        s = self._scan(data, size, **kwargs)
        result = [x for x in s]
        assert result == eResult, result
        assert len(s) == len(eResult)


class test_clockwise_all_corners(verify_all_corners):
    def test_tl(self):
        corner = 'tl'
        self.execute_4x4(corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (2, 3), (3, 3), (3, 2), (3, 1),
                                                 (3, 0), (2, 0), (1, 0), (1, 1), (1, 2), (2, 2), (2, 1)])
        self.execute_4x3(corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (1, 2), (2, 2), (3, 2), (3, 1), (3, 0), (2, 0),
                                                 (1, 0), (1, 1), (2, 1)])
        self.execute_4x2(corner=corner, eResult=[(0, 0), (0, 1), (1, 1), (2, 1), (3, 1), (3, 0), (2, 0), (1, 0)])
        self.execute_4x1(corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (3, 0)])
        self.execute_1x1(corner=corner, eResult=[(0, 0)])
        self.execute_1x2(corner=corner, eResult=[(0, 0), (0, 1)])
        self.execute_1x3(corner=corner, eResult=[(0, 0), (0, 1), (0, 2)])
        self.execute_1x4(corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (0, 3)])
        self.execute_2x1(corner=corner, eResult=[(0, 0), (1, 0)])
        self.execute_2x2(corner=corner, eResult=[(0, 0), (0, 1), (1, 1), (1, 0)])
        self.execute_2x3(corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (1, 2), (1, 1), (1, 0)])
        self.execute_2x4(corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (1, 2), (1, 1), (1, 0)])
        self.execute_3x1(corner=corner, eResult=[(0, 0), (1, 0), (2, 0)])
        self.execute_3x2(corner=corner, eResult=[(0, 0), (0, 1), (1, 1), (2, 1), (2, 0), (1, 0)])
        self.execute_3x3(corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (1, 2), (2, 2), (2, 1), (2, 0), (1, 0), (1, 1)])
        self.execute_3x4(corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (2, 3), (2, 2), (2, 1), (2, 0),
                                                 (1, 0), (1, 1), (1, 2)])

    def test_tr(self):
        corner = 'tr'
        self.execute_4x4(corner=corner, eResult=[(0, 3), (1, 3), (2, 3), (3, 3), (3, 2), (3, 1), (3, 0), (2, 0), (1, 0),
                                                 (0, 0), (0, 1), (0, 2), (1, 2), (2, 2), (2, 1), (1, 1) ])
        self.execute_4x3(corner=corner, eResult=[(0, 2), (1, 2), (2, 2), (3, 2), (3, 1), (3, 0), (2, 0), (1, 0), (0, 0),
                                                 (0, 1), (1, 1), (2, 1)])
        self.execute_4x2(corner=corner, eResult=[(0, 1), (1, 1), (2, 1), (3, 1), (3, 0), (2, 0), (1, 0), (0, 0)])
        self.execute_4x1(corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (3, 0)])
        self.execute_1x1(corner=corner, eResult=[(0, 0)])
        self.execute_1x2(corner=corner, eResult=[(0, 1), (0, 0)])
        self.execute_1x3(corner=corner, eResult=[(0, 2), (0, 1), (0, 0) ])
        self.execute_1x4(corner=corner, eResult=[(0, 3), (0, 2), (0, 1), (0, 0)])
        self.execute_2x1(corner=corner, eResult=[(0, 0), (1, 0)])
        self.execute_2x2(corner=corner, eResult=[(0, 1), (1, 1), (1, 0), (0, 0)])
        self.execute_2x3(corner=corner, eResult=[(0, 2), (1, 2), (1, 1), (1, 0), (0, 0), (0, 1)])
        self.execute_2x4(corner=corner, eResult=[(0, 3), (1, 3), (1, 2), (1, 1), (1, 0), (0, 0), (0, 1), (0, 2)])
        self.execute_3x1(corner=corner, eResult=[(0, 0), (1, 0), (2, 0)])
        self.execute_3x2(corner=corner, eResult=[(0, 1), (1, 1), (2, 1), (2, 0), (1, 0), (0, 0)])
        self.execute_3x3(corner=corner, eResult=[(0, 2), (1, 2), (2, 2), (2, 1), (2, 0), (1, 0), (0, 0), (0, 1), (1, 1)])
        self.execute_3x4(corner=corner, eResult=[(0, 3), (1, 3), (2, 3), (2, 2), (2, 1), (2, 0), (1, 0), (0, 0), (0, 1),
                                                 (0, 2), (1, 2), (1, 1)])

    def test_br(self):
        corner = 'br'
        self.execute_4x4(corner=corner, eResult=[(3, 3), (3, 2), (3, 1), (3, 0), (2, 0), (1, 0), (0, 0), (0, 1), (0, 2),
                                                 (0, 3), (1, 3), (2, 3), (2, 2), (2, 1), (1, 1), (1, 2)])
        self.execute_4x3(corner=corner, eResult=[(3, 2), (3, 1), (3, 0), (2, 0), (1, 0), (0, 0), (0, 1), (0, 2), (1, 2),
                                                 (2, 2), (2, 1), (1, 1)])
        self.execute_4x2(corner=corner, eResult=[(3, 1), (3, 0), (2, 0), (1, 0), (0, 0), (0, 1), (1, 1), (2, 1)])
        self.execute_4x1(corner=corner, eResult=[(3, 0), (2, 0), (1, 0), (0, 0)])
        self.execute_1x1(corner=corner, eResult=[(0, 0)])
        self.execute_1x2(corner=corner, eResult=[(0, 1), (0, 0)])
        self.execute_1x3(corner=corner, eResult=[(0, 2), (0, 1), (0, 0) ])
        self.execute_1x4(corner=corner, eResult=[(0, 3), (0, 2), (0, 1), (0, 0)])
        self.execute_2x1(corner=corner, eResult=[(1, 0), (0, 0)])
        self.execute_2x2(corner=corner, eResult=[(1, 1), (1, 0), (0, 0), (0, 1)])
        self.execute_2x3(corner=corner, eResult=[(1, 2), (1, 1), (1, 0), (0, 0), (0, 1), (0, 2)])
        self.execute_2x4(corner=corner, eResult=[(1, 3), (1, 2), (1, 1), (1, 0), (0, 0), (0, 1), (0, 2), (0, 3)])
        self.execute_3x1(corner=corner, eResult=[(2, 0), (1, 0), (0, 0)])
        self.execute_3x2(corner=corner, eResult=[(2, 1), (2, 0), (1, 0), (0, 0), (0, 1), (1, 1)])
        self.execute_3x3(corner=corner, eResult=[(2, 2), (2, 1), (2, 0), (1, 0), (0, 0), (0, 1), (0, 2), (1, 2), (1, 1)])
        self.execute_3x4(corner=corner, eResult=[(2, 3), (2, 2), (2, 1), (2, 0), (1, 0), (0, 0), (0, 1), (0, 2), (0, 3),
                                                 (1, 3), (1, 2), (1, 1)])

    def test_bl(self):
        corner = 'bl'
        self.execute_4x4(corner=corner, eResult=[(3, 0), (2, 0), (1, 0), (0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (2, 3),
                                                 (3, 3), (3, 2), (3, 1), (2, 1), (1, 1), (1, 2), (2, 2)])
        self.execute_4x3(corner=corner, eResult=[(3, 0), (2, 0), (1, 0), (0, 0), (0, 1), (0, 2), (1, 2), (2, 2), (3, 2),
                                                 (3, 1), (2, 1), (1, 1)])
        self.execute_4x2(corner=corner, eResult=[(3, 0), (2, 0), (1, 0), (0, 0), (0, 1), (1, 1), (2, 1), (3, 1)])
        self.execute_4x1(corner=corner, eResult=[(3, 0), (2, 0), (1, 0), (0, 0)])
        self.execute_1x1(corner=corner, eResult=[(0, 0)])
        self.execute_1x2(corner=corner, eResult=[(0, 0), (0, 1)])
        self.execute_1x3(corner=corner, eResult=[(0, 0), (0, 1), (0, 2)])
        self.execute_1x4(corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (0, 3)])
        self.execute_2x1(corner=corner, eResult=[(1, 0), (0, 0)])
        self.execute_2x2(corner=corner, eResult=[(1, 0), (0, 0), (0, 1), (1, 1)])
        self.execute_2x3(corner=corner, eResult=[(1, 0), (0, 0), (0, 1), (0, 2), (1, 2), (1, 1)])
        self.execute_2x4(corner=corner, eResult=[(1, 0), (0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (1, 2), (1, 1)])
        self.execute_3x1(corner=corner, eResult=[(2, 0), (1, 0), (0, 0)])
        self.execute_3x2(corner=corner, eResult=[(2, 0), (1, 0), (0, 0), (0, 1), (1, 1), (2, 1)])
        self.execute_3x3(corner=corner, eResult=[(2, 0), (1, 0), (0, 0), (0, 1), (0, 2), (1, 2), (2, 2), (2, 1), (1, 1)])
        self.execute_3x4(corner=corner, eResult=[(2, 0), (1, 0), (0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (2, 3), (2, 2),
                                                 (2, 1), (1, 1), (1, 2)])

class test_counterclockwise_all_corners(verify_all_corners):
    def test_tl(self):
        corner = 'tl'
        direction = SpirolDirection.COUNTER_CLOCK
        self.execute_4x4(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (3, 0), (3, 1), (3, 2),
                                                                      (3, 3), (2, 3), (1, 3), (0, 3), (0, 2), (0, 1),
                                                                      (1, 1), (2, 1), (2, 2), (1, 2)])
        self.execute_4x3(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (3, 0), (3, 1), (3, 2),
                                                                      (2, 2), (1, 2), (0, 2), (0, 1), (1, 1), (2, 1)])
        self.execute_4x2(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (3, 0), (3, 1), (2, 1),
                                                                      (1, 1), (0, 1)])
        self.execute_4x1(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (3, 0)])
        self.execute_1x1(direction=direction, corner=corner, eResult=[(0, 0)])
        self.execute_1x2(direction=direction, corner=corner, eResult=[(0, 0), (0, 1)])
        self.execute_1x3(direction=direction, corner=corner, eResult=[(0, 0), (0, 1), (0, 2)])
        self.execute_1x4(direction=direction, corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (0, 3)])
        self.execute_2x1(direction=direction, corner=corner, eResult=[(0, 0), (1, 0)])
        self.execute_2x2(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (1, 1), (0, 1)])
        self.execute_2x3(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (1, 1), (1, 2), (0, 2), (0, 1)])
        self.execute_2x4(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (1, 1), (1, 2), (1, 3), (0, 3),
                                                                      (0, 2), (0, 1)])
        self.execute_3x1(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0)])
        self.execute_3x2(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (2, 1), (1, 1), (0, 1)])
        self.execute_3x3(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (2, 1), (2, 2), (1, 2),
                                                                      (0, 2), (0, 1), (1, 1)])
        self.execute_3x4(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (2, 1), (2, 2), (2, 3),
                                                                      (1, 3), (0, 3), (0, 2), (0, 1), (1, 1), (1, 2)])

    def test_tr(self):
        corner = 'tr'
        direction = SpirolDirection.COUNTER_CLOCK
        self.execute_4x4(direction=direction, corner=corner, eResult=[(0, 3), (0, 2), (0, 1), (0, 0), (1, 0), (2, 0),
                                                                      (3, 0), (3, 1), (3, 2), (3, 3), (2, 3), (1, 3),
                                                                      (1, 2), (1, 1), (2, 1), (2, 2)])
        self.execute_4x3(direction=direction, corner=corner, eResult=[(0, 2), (0, 1), (0, 0), (1, 0), (2, 0), (3, 0),
                                                                      (3, 1), (3, 2), (2, 2), (1, 2), (1, 1), (2, 1)])
        self.execute_4x2(direction=direction, corner=corner, eResult=[(0, 1), (0, 0), (1, 0), (2, 0), (3, 0), (3, 1),
                                                                      (2, 1), (1, 1)])
        self.execute_4x1(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0), (3, 0)])
        self.execute_1x1(direction=direction, corner=corner, eResult=[(0, 0)])
        self.execute_1x2(direction=direction, corner=corner, eResult=[(0, 1), (0, 0)])
        self.execute_1x3(direction=direction, corner=corner, eResult=[(0, 2), (0, 1), (0, 0)])
        self.execute_1x4(direction=direction, corner=corner, eResult=[(0, 3), (0, 2), (0, 1), (0, 0)])
        self.execute_2x1(direction=direction, corner=corner, eResult=[(0, 0), (1, 0)])
        self.execute_2x2(direction=direction, corner=corner, eResult=[(0, 1), (0, 0), (1, 0), (1, 1)])
        self.execute_2x3(direction=direction, corner=corner, eResult=[(0, 2), (0, 1), (0, 0), (1, 0), (1, 1), (1, 2)])
        self.execute_2x4(direction=direction, corner=corner, eResult=[(0, 3), (0, 2), (0, 1), (0, 0), (1, 0), (1, 1),
                                                                      (1, 2), (1, 3)])
        self.execute_3x1(direction=direction, corner=corner, eResult=[(0, 0), (1, 0), (2, 0)])
        self.execute_3x2(direction=direction, corner=corner, eResult=[(0, 1), (0, 0), (1, 0), (2, 0), (2, 1), (1, 1)])
        self.execute_3x3(direction=direction, corner=corner, eResult=[(0, 2), (0, 1), (0, 0), (1, 0), (2, 0), (2, 1),
                                                                      (2, 2), (1, 2), (1, 1)])
        self.execute_3x4(direction=direction, corner=corner, eResult=[(0, 3), (0, 2), (0, 1), (0, 0), (1, 0), (2, 0),
                                                                      (2, 1), (2, 2), (2, 3), (1, 3), (1, 2), (1, 1)])

    def test_br(self):
        corner = 'br'
        direction = SpirolDirection.COUNTER_CLOCK
        self.execute_4x4(direction=direction, corner=corner, eResult=[(3, 3), (2, 3), (1, 3), (0, 3), (0, 2), (0, 1),
                                                                      (0, 0), (1, 0), (2, 0), (3, 0), (3, 1), (3, 2),
                                                                      (2, 2), (1, 2), (1, 1), (2, 1)])
        self.execute_4x3(direction=direction, corner=corner, eResult=[(3, 2), (2, 2), (1, 2), (0, 2), (0, 1), (0, 0),
                                                                      (1, 0), (2, 0), (3, 0), (3, 1), (2, 1), (1, 1)])
        self.execute_4x2(direction=direction, corner=corner, eResult=[(3, 1), (2, 1), (1, 1), (0, 1), (0, 0), (1, 0),
                                                                      (2, 0), (3, 0)])
        self.execute_4x1(direction=direction, corner=corner, eResult=[(3, 0), (2, 0), (1, 0), (0, 0)])
        self.execute_1x1(direction=direction, corner=corner, eResult=[(0, 0)])
        self.execute_1x2(direction=direction, corner=corner, eResult=[(0, 1), (0, 0)])
        self.execute_1x3(direction=direction, corner=corner, eResult=[(0, 2), (0, 1), (0, 0)])
        self.execute_1x4(direction=direction, corner=corner, eResult=[(0, 3), (0, 2), (0, 1), (0, 0)])
        self.execute_2x1(direction=direction, corner=corner, eResult=[(1, 0), (0, 0)])
        self.execute_2x2(direction=direction, corner=corner, eResult=[(1, 1), (0, 1), (0, 0), (1, 0)])
        self.execute_2x3(direction=direction, corner=corner, eResult=[(1, 2), (0, 2), (0, 1), (0, 0), (1, 0), (1, 1)])
        self.execute_2x4(direction=direction, corner=corner, eResult=[(1, 3), (0, 3), (0, 2), (0, 1), (0, 0), (1, 0),
                                                                      (1, 1), (1, 2)])
        self.execute_3x1(direction=direction, corner=corner, eResult=[(2, 0), (1, 0), (0, 0)])
        self.execute_3x2(direction=direction, corner=corner, eResult=[(2, 1), (1, 1), (0, 1), (0, 0), (1, 0), (2, 0)])
        self.execute_3x3(direction=direction, corner=corner, eResult=[(2, 2), (1, 2), (0, 2), (0, 1), (0, 0), (1, 0),
                                                                      (2, 0), (2, 1), (1, 1)])
        self.execute_3x4(direction=direction, corner=corner, eResult=[(2, 3), (1, 3), (0, 3), (0, 2), (0, 1), (0, 0),
                                                                      (1, 0), (2, 0), (2, 1), (2, 2), (1, 2), (1, 1)])

    def test_bl(self):
        corner = 'bl'
        direction = SpirolDirection.COUNTER_CLOCK
        self.execute_4x4(direction=direction, corner=corner, eResult=[(3, 0), (3, 1), (3, 2), (3, 3), (2, 3), (1, 3),
                                                                      (0, 3), (0, 2), (0, 1), (0, 0), (1, 0), (2, 0),
                                                                      (2, 1), (2, 2), (1, 2), (1, 1)])
        self.execute_4x3(direction=direction, corner=corner, eResult=[(3, 0), (3, 1), (3, 2), (2, 2), (1, 2), (0, 2),
                                                                      (0, 1), (0, 0), (1, 0), (2, 0), (2, 1), (1, 1)])
        self.execute_4x2(direction=direction, corner=corner, eResult=[(3, 0), (3, 1), (2, 1), (1, 1), (0, 1), (0, 0),
                                                                      (1, 0), (2, 0)])
        self.execute_4x1(direction=direction, corner=corner, eResult=[(3, 0), (2, 0), (1, 0), (0, 0)])
        self.execute_1x1(direction=direction, corner=corner, eResult=[(0, 0)])
        self.execute_1x2(direction=direction, corner=corner, eResult=[(0, 0), (0, 1)])
        self.execute_1x3(direction=direction, corner=corner, eResult=[(0, 0), (0, 1), (0, 2)])
        self.execute_1x4(direction=direction, corner=corner, eResult=[(0, 0), (0, 1), (0, 2), (0, 3)])
        self.execute_2x1(direction=direction, corner=corner, eResult=[(1, 0), (0, 0)])
        self.execute_2x2(direction=direction, corner=corner, eResult=[(1, 0), (1, 1), (0, 1), (0, 0)])
        self.execute_2x3(direction=direction, corner=corner, eResult=[(1, 0), (1, 1), (1, 2), (0, 2), (0, 1), (0, 0)])
        self.execute_2x4(direction=direction, corner=corner, eResult=[(1, 0), (1, 1), (1, 2), (1, 3), (0, 3), (0, 2),
                                                                      (0, 1), (0, 0)])
        self.execute_3x1(direction=direction, corner=corner, eResult=[(2, 0), (1, 0), (0, 0)])
        self.execute_3x2(direction=direction, corner=corner, eResult=[(2, 0), (2, 1), (1, 1), (0, 1), (0, 0), (1, 0)])
        self.execute_3x3(direction=direction, corner=corner, eResult=[(2, 0), (2, 1), (2, 2), (1, 2), (0, 2), (0, 1),
                                                                      (0, 0), (1, 0), (1, 1)])
        self.execute_3x4(direction=direction, corner=corner, eResult=[(2, 0), (2, 1), (2, 2), (2, 3), (1, 3), (0, 3),
                                                                      (0, 2), (0, 1), (0, 0), (1, 0), (1, 1), (1, 2)])

if __name__ == '__main__':
    unittest.main()
