#!/usr/bin/env python
#    -*- coding: utf-8 -*-

import unittest

from spirol import spirol
from spirol.errors import InputFormatError, UnknownImplementation
from spirol.impl_factory import spirol_factory
from spirol.utils import SpirolType


class test_successful_iterations_scan(unittest.TestCase):

    def setUp(self, verbose=False, debugging=False, method='scan', corner='tl', direction='clockwise'):
        self._debugging = debugging
        self._verbose = verbose
        self._method = method
        self._corner = corner
        self._direction = direction

    def tearDown(self):
        pass

    def _scan(self, data, size, **kwargs):
        return spirol(data, size, verbose=self._verbose, debug=self._debugging, **kwargs).scan()

    def _iterate(self, data, size, **kwargs):
        return spirol(data, size, verbose=self._verbose, debug=self._debugging, **kwargs)

    def _execute(self, data, size, corner=None, direction=None):
        return self._scan(data, size, corner=corner or self._corner, direction=direction or self._direction) if \
            self._method == 'scan' else self._iterate(data, size, corner=corner or self._corner, direction=direction or
                                                                                                        self._direction)

    def test_1x1(self, size=(1, 1)):
        data = [[1]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_1x2(self, size=(1, 2)):
        data = [[1, 2]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_1x3(self, size=(1, 3)):
        data = [[1, 2, 3]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_1x4(self, size=(1, 4)):
        data = [[1, 2, 3, 4]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3, 4]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_2x1(self, size=(2, 1)):
        data = [[1], [2]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_2x2(self, size=(2, 2)):
        data = [[1, 2], [3, 4]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 4, 3]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_2x3(self, size=(2, 3)):
        data = [[1, 2, 3], [4, 5, 6]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3, 6, 5, 4]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_2x4(self, size=(2, 4)):
        data = [[1, 2, 3, 4], [5, 6, 7, 8]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3, 4, 8, 7, 6, 5]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_3x1(self, size=(3, 1)):
        data = [[1], [2], [3]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_3x2(self, size=(3, 2)):
        data = [[1, 2], [3, 4], [5, 6]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 4, 6, 5, 3]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_3x3(self, size=(3, 3)):
        data = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3, 6, 9, 8, 7, 4, 5]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_3x4(self, size=(3, 4)):
        data = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_4x1(self, size=(4, 1)):
        data = [[1], [2], [3], [4]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3, 4]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_4x2(self, size=(4, 2)):
        data = [[1, 2], [3, 4], [5, 6], [7, 8]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 4, 6, 8, 7, 5, 3]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_4x3(self, size=(4, 3)):
        data = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3, 6, 9, 12, 11, 10, 7, 4, 5, 8]
        assert result == eResult, result
        assert len(s) == len(eResult)

    def test_4x4(self, size=(4, 4)):
        data = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
        s = self._execute(data, size)
        result = [x for x in s]
        eResult = [1, 2, 3, 4, 8, 12, 16, 15, 14, 13, 9, 5, 6, 7, 11, 10]
        assert result == eResult, result
        assert len(s) == len(eResult)


class test_successful_iterations_iter(test_successful_iterations_scan):
    def setUp(self, *args, **kwargs):
        kwargs['method'] = 'iter'
        test_successful_iterations_scan.setUp(self, *args, **kwargs)

class test_input_format_errors_scan(test_successful_iterations_scan):

    def test_1x1(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_1x1(self, size=(2, 5)))

    def test_1x2(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_1x2(self, size=(2, 5)))

    def test_1x3(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_1x3(self, size=(2, 5)))

    def test_1x4(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_1x4(self, size=(2, 5)))

    def test_2x1(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_2x1(self, size=(2, 5)))

    def test_2x2(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_2x2(self, size=(2, 5)))

    def test_2x3(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_2x3(self, size=(2, 5)))

    def test_2x4(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_2x4(self, size=(2, 5)))

    def test_3x1(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_3x1(self, size=(2, 5)))

    def test_3x2(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_3x2(self, size=(2, 5)))

    def test_3x3(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_3x3(self, size=(2, 5)))

    def test_3x4(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_3x4(self, size=(2, 5)))

    def test_4x1(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_4x1(self, size=(2, 5)))

    def test_4x2(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_4x2(self, size=(2, 5)))

    def test_4x3(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_4x3(self, size=(2, 5)))

    def test_4x4(self):
        self.assertRaises(InputFormatError, lambda: test_successful_iterations_scan.test_4x4(self, size=(2, 5)))


class test_input_format_errors_iter(test_input_format_errors_scan):
    def setUp(self, *args, **kwargs):
        kwargs['method'] = 'iter'
        test_input_format_errors_scan.setUp(self, *args, **kwargs)


class test_str_core(unittest.TestCase):

    def setUp(self, verbose=False, debugging=False):
        self._debugging = debugging
        self._verbose = verbose

    def tearDown(self):
        pass

    def test_str_all(self, size=(3, 3)):
        data = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        result = str(spirol(data, size, verbose=self._verbose, debug=self._debugging))
        eResult = 'spirol(3, 3, clockwise from tl): [1, 2, 3, 6, 9, 8, 7, 4, 5]'
        assert result == eResult, result

    def test_str_elipsis(self, size=(4, 4)):
        data = [[1, 2, 3, 4],
                   [5, 6, 7, 8],
                   [9, 10, 11, 12],
                   [13, 14, 15, 16]]
        result = str(spirol(data, size, verbose=self._verbose, debug=self._debugging))
        eResult = 'spirol(4, 4, clockwise from tl): [1, 2, 3, 4, 8...5, 6, 7, 11, 10]'
        assert result == eResult, result


class test_spirol_factory(unittest.TestCase):

    def test_UnknownImplementationError(self):
        t = 'voo'
        self.assertRaises(UnknownImplementation, lambda: SpirolType.validate(t))
        self.assertRaises(UnknownImplementation, lambda: spirol_factory(t))

    def test_UnknownImplementationError(self):
        t = 'voo'
        self.assertRaises(UnknownImplementation, lambda: SpirolType.validate(t))
        self.assertRaises(UnknownImplementation, lambda: spirol_factory(t))


if __name__ == '__main__':
    unittest.main()
